import os
import sys
import unittest
from linguistic_model_testing.worker.singleWebDriver import JIAMWebDriver

class TestJIAMSizes(unittest.TestCase):

    def setUp(self):
        url = 'http://www.psychology.nottingham.ac.uk/staff/wvh/jiam/startjiam.html'
        lexicon_name = 'lexx3to5_100.txt'
        path_to_lexicon = os.getcwd()
        path_to_output = ""
        self._webdriver = JIAMWebDriver(url=url,
                            lexicon_name=lexicon_name,
                            path_to_lexicon=path_to_lexicon,
                            path_to_output=path_to_output,
                            headless=True)
    def tearDown(self):
        pass

    def test_big_lexicon(self):
        self._webdriver.set_lexicon_file('lexx_all_200000.txt')
        resp = self._webdriver.getRespTimeOfLexicon()
        self.assertIsInstance(resp, int)
        # todo assert the range

    def test_small_lexicon(self):
        self._webdriver.set_lexicon_file('lexx3to5_100.txt')
        resp = self._webdriver.getRespTimeOfLexicon()
        self.assertIsInstance(resp, int)
        # todo assert the range

    def test_medium_lexicon(self):
        self._webdriver.set_lexicon_file('lexx3to5_n5000.txt')
        resp = self._webdriver.getRespTimeOfLexicon()
        self.assertIsInstance(resp, int)
        # todo assert the range

if __name__ == '__main__':
    unittest.main()