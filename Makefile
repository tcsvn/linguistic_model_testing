install:
	docker pull	registry.gitlab.com/tcsvn/linguistic_model_testing/orchestration:latest
	docker pull	registry.gitlab.com/tcsvn/linguistic_model_testing/worker:latest
	rm -rf testing
	rm -rf linguistic_model_testing
	rm -rf docker
	rm -rf .git
	rm setup.py MANIFEST.in .gitlab-ci.yml .gitignore LICENSE
