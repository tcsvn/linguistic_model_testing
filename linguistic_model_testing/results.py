import pandas as pd
import glob
import os
import numpy as np
from matplotlib.pyplot import savefig
from scipy.optimize import curve_fit
from scipy.optimize import least_squares
from matplotlib import pyplot as plt

# this application accumulates all results the folder off the given path, where the results lie
names=['wordcount', 'time']
path = os.getcwd() + '/realresults/'
all_files = glob.glob(path + "*.csv")

# read in files and append to one big dataframe
res = pd.read_csv(all_files.pop(), sep=" ", names=names)
for file in all_files:
    res =res.append(pd.read_csv(file, sep=" ", names=names))

# mean of time over wordcount
res = res.groupby(['wordcount'], as_index=False).mean()


#res.plot(x=res['wordcount'], y=res['time'], kind='scatter')
mat = res.as_matrix()
x = mat[:,0]
y = mat[:,1]
def expon(x, a, b, c):
    return a + b*np.exp(x*-c)

def quad(x, a, b, c):
    return b*np.power(x, c) + a

init_guess = (-634495, 613109, -0.000163424)
best_vals_exp, covar_exp = curve_fit(expon,  x, y, p0=init_guess)

init_guess = (0, 0.3, 2)
best_vals_quad, covar_quad = curve_fit(quad, x, y, p0=init_guess)

# plot in diagram
# create exp x,y
x_test = np.linspace(0, 10000)
y_exp = expon(x_test, best_vals_exp[0], best_vals_exp[1], best_vals_exp[2])
y_quad = quad(x_test, best_vals_quad[0], best_vals_quad[1], best_vals_quad[2])

print("-"*300)
print("used exponential fct: f(x) = a + b * exp(x*-c)")
print("\ta: " + str(best_vals_exp[0]))
print("\tb: " + str(best_vals_exp[1]))
print("\tc: " + str(best_vals_exp[2]))
print("-------")
var = np.sqrt(np.diag(covar_exp))
error = 1/3 * (var[0] + var[1] + var[2])
print("standard dev for each param: " + str(var))
print("error:\t\t\t" + str(error))


print("-"*300)
print("used quad fct: f(x) = a + b *(x^c)")
print("\ta: " + str(best_vals_quad[0]))
print("\tb: " + str(best_vals_quad[1]))
print("\tc: " + str(best_vals_quad[2]))
print("-------")
var = np.sqrt(np.diag(covar_quad))
error = 1/3 * (var[0] + var[1] + var[2])
print("standard dev for each param: " + str(var))
print("error:\t\t\t" + str(error))


plt.plot(x, y, 'o', markersize=4, label='data')
plt.plot(x_test, y_exp, label='fitted exp model')
plt.plot(x_test, y_quad, label='fitted quad model')
plt.xlabel('wordcount')
plt.ylabel('ms')
plt.legend(loc='lower right')
savefig('results.png')
plt.show()
