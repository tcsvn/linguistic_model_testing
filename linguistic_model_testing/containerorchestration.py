import docker
import os
import time

class Dirigent():
    def __init__(self,
        stepList,
        lexicon_name,
        hostPathToLexicaFolder = "/lexica",
        hostPathToResultFolder = '/results',
        url = "http://www.psychology.nottingham.ac.uk/staff/wvh/jiam/startjiam.html",
        result_file = "results.txt"):

        self._lexicon_name = lexicon_name
        self._stepList = stepList
        self._maxContainerParallel = 3
        self._container_running = 0
        self._url = url
        self._containerList = []

        self._image_name = 'registry.gitlab.com/tcsvn/linguistic_model_testing/worker:latest'
        self._host_lexica_folder = hostPathToLexicaFolder
        self._host_result_folder = hostPathToResultFolder

        self._worker_lexica_folder = "/lexica"
        self._worker_result_folder = "/results"
        self._result_file = result_file
        self._client = docker.from_env()


    def __str__(self):
        s = "Docker dirigent"
        s = s + "worker_lexica_folder:\t" + self._worker_lexica_folder + "\n"
        s = s + "worker_result_folder:\t" + self._worker_result_folder + "\n"
        s = s + "host_result_folder:\t" + self._host_result_folder + "\n"
        return s

    def build_worker_image(self):
        print("building image")
        cwd = os.getcwd()
        self._client.images.build(
            path = '%s/worker'%(cwd),
            pull=True,
            nocache = True,
            tag = self._image_name)

    # if a container with the same name exists before
    # then remove the container
    def cleanup_container(self):
        for item in self._stepList:
            try:
                name = "lingu_%s_%s"%(self._lexicon_name, item)
                cont = self._client.containers.get(name)
                cont.remove(v=True)
            except:
                pass

    def create_containers(self):
        self.cleanup_container()
        for item in self._stepList:
            lexicon_file = "%s_%s.txt"%(self._lexicon_name, item)
            environmental_vars = [
                "PO=%s/%s"%(self._worker_result_folder, self._result_file),
                "PL=%s/%s"%(self._worker_lexica_folder, lexicon_file),
                "WC=%s"%(item),
                "URL=%s"%(self._url)]

            volumes = {
                # todo debug, comment the first out to restore docker case
                self._host_lexica_folder: {'bind' : self._worker_lexica_folder, 'mode': 'rw'},
                self._host_result_folder: {'bind' : self._worker_result_folder, 'mode': 'rw'}
            }

            self._containerList.append(
                self._client.containers.create(
                    name="lingu_%s_%s"%(self._lexicon_name, item),
                    image=self._image_name,
                    environment=environmental_vars,
                    volumes=volumes
                    # set cpu amount cpu_percent = 0.25
                )
            )

    def getAmountContainerRunning(self):
        count = 0
        for container in self._containerList:
            if(container.status == 'running'):
                count = count + 1
        return count

    def startNextContainer(self):
        for container in self._containerList:
            # if container not started yet
            con = self._client.containers.get(container.name)
            if (con.status == 'created'):
                container.start()
                break

    # has to be made as the data is not updated correctly
    def updateList(self):
        for idx, container in enumerate(self._containerList):
            con = self._client.containers.get(container.name)
            self._containerList.remove(container)
            self._containerList.insert(idx, con)



    def removeExitedContainersFromList(self):
        for container in self._containerList:
            if (container.status == 'exited'):
                self._containerList.remove(container)

    def start_containers(self):
        num_start = 0
        for container in self._containerList:
            container.start()
            if self._maxContainerParallel == num_start:
                break
            num_start = num_start +1

        print('started containers')
        # arbeite alle container ab
        while self._containerList:
            print(str(self._containerList))
            self.updateList()
            # when there are still unprocessed containers
            # and the amount running is lesser than
            if (self._maxContainerParallel > self.getAmountContainerRunning()
                and self._maxContainerParallel < len(self._containerList)):
                self.startNextContainer()
            self.removeExitedContainersFromList()
            time.sleep(5)
        #self.cleanup_container()

