import pandas as pd
import os
class LexiconHandler():
    def __init__(self, lexx_name='lexx_all',
                 pathToOriginalLexicon='lexx.txt',
                 pathToLexicaFolder='',
                 stepsize=1000,
                 stepList=[]):

        self._stepsize = stepsize
        self._steplist = stepList
        self._lexx_name = lexx_name
        self._pathToLexicaFolder = pathToLexicaFolder

        # init data frame
        self._df = pd.read_csv(pathToOriginalLexicon, sep=' ')
        self._df[['Frequency']] = self._df[['Frequency']].apply(pd.to_numeric)
        # shuffle data of lexx.txt for other examples
        self._df = self._df.sample(frac=1).reset_index(drop=True)

    def getPathToLexicaFolder(self):
        return self._pathToLexicaFolder

    def getLexiconLength(self):
        return self._df.count()[0]
    # gets an intervall and sets the lexicon to the sizes

    def set_wordLength(self, intervall=[3,6]):
        self._df = self._df[self._df['Words'].str.len() >= intervall[0]]
        self._df = self._df[self._df['Words'].str.len() <= intervall[1]]

    def generateLexicas(self):
        for i in self._steplist:
            if i <= self.getLexiconLength():
                tmpdf = self._df.head(i)
                tmpdf.to_csv('%s/%s_%s.txt'%(self._pathToLexicaFolder, self._lexx_name, i), sep = ' ', header=False, index = False)
                #print('saving %s/%s_%s.txt'%(self._pathToLexicaFolder, self._lexx_name, i))
            else:
                print("count is greater than dataframe length")

    def deleteLexicas(self):
        for i in self._steplist:
            if i <= self.getLexiconLength():
                os.remove('%s/%s_%s.txt'%(self._pathToLexicaFolder, self._lexx_name, i))