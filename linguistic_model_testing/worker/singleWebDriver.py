from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import *
from selenium.webdriver.support.select import Select
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from selenium.webdriver.firefox.options import Options
import time

class JIAMWebDriver():
    def __init__(self, url='http://www.psychology.nottingham.ac.uk/staff/wvh/jiam/startjiam.html',
                 lexicon_name="lexx3to6_100.txt",
                 path_to_lexicon="/home/chris/PycharmProjects/linguistic/src",
                 path_to_output = "/home/chris/PycharmProjects/linguistic",
                 headless = True
                 ):
        self._url=url
        self._lexicon_name = lexicon_name
        self._path = path_to_lexicon + "/" + lexicon_name
        self._path_download = path_to_output

        # init driver
        profile = self.createFirefoxProfile()
        if (headless):
            options = Options()
            options.add_argument("--headless")
            self._driver = webdriver.Firefox(firefox_options=options)
        else:
            self._driver = webdriver.Firefox(profile)

        self._driver.get(self._url)

    def __str__(self):
        s = "-"*100 + "\n"
        s = s + "url:\t\t\t" + self._url + "\n"
        s = s + "lexicon:\t\t" + self._lexicon_name + "\n"
        s = s + "lexiconfolder:\t" + self._path + "\n"
        s = s + "outputfolder:\t" + self._path_download + "\n"
        s = s + "-"* 100
        return s

    def createFirefoxProfile(self):
        # create firefox profile to specify download behaviour
        profile = FirefoxProfile()
        profile.set_preference("browser.download.folderList", 2)
        profile.set_preference("browser.download.dir", self._path_download)
        profile.set_preference("browser.download.manager.showWhenStarting", False)
        profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "text/*")
        return profile

    def set_lexicon_file(self, name):
        self._lexicon_name = str(name)

    # page interaction
    def uploadOwnLexicon(self):
        # upload lexicon
        input = WebDriverWait(driver=self._driver, timeout=10).until(EC.presence_of_element_located((By.ID, "theSelectedFile")))
        input.send_keys(self._path)

        # select right lexicon
        try:
           WebDriverWait(driver=self._driver, timeout=3).until(EC.alert_is_present(),
                                                         'Timed out waiting for alert creat')
           alert = self._driver.switch_to.alert
           alert.accept()
        except TimeoutException:
            print("no alert was present")

        # select our lexicon
        try:
            for item in self._driver.find_elements_by_xpath("//select"):
                if self._lexicon_name in item.text:
                    select = Select(item)
                    select.select_by_visible_text(self._lexicon_name)
        except:
            print("could not select our lexicon: " + self._lexicon_name)


    # deselect drawing of activations during simulation
    def deactivateVerboseOutput(self):
        checkbox = self._driver.find_element_by_id("graphics_feedback_batch")
        if checkbox.is_selected():
            checkbox.click()
        else:
            print("checkbox is not selected")

    def clickOnJavaScriptButton(self, button_name):
        elements = self._driver.find_elements_by_xpath("//div[contains(text(),'%s')]"%(button_name))
        button = None
        for item in elements:
            if (item.is_displayed()):
                button = item
        button.click()


    def navigateFromFrontPageToSettings(self):
        element = self._driver.find_element_by_id("bParameter")
        element.click()

    def navigateFromSettingsToFrontPage(self):
        self.clickOnJavaScriptButton("Back")

    def createModel(self):
        element = self._driver.find_element_by_id("bCreateModel")
        element.click()
        # todo check if working
        # wait until the model is created
        while not EC.presence_of_element_located((By.ID, "bAdvancedInput")):
            time.sleep(1)

    def setInputToLexicon(self):
        # set input
        element = WebDriverWait(driver=self._driver, timeout=10).until(EC.presence_of_element_located((By.ID, "bAdvancedInput")))
        element.click()
        input = WebDriverWait(driver=self._driver, timeout=10).until(EC.presence_of_element_located((By.ID, "theSelectedBatchFile")))
        input.send_keys(self._path)
        time.sleep(1)
        self.clickOnJavaScriptButton('Ok')

    def runSimulation(self):
        start = WebDriverWait(driver=self._driver, timeout=10).until(EC.presence_of_element_located((By.ID, "bStart")))
        start.click()
        while not start.is_displayed():
            time.sleep(1)

    def debugInfLoop(self):
        while True:
            time.sleep(1)

    def downloadOutput(self):
        # click on download
        saveOutput = self._driver.find_element_by_xpath("//button[contains(text(),'Save Console Output')]")
        saveOutput.click()

    def getOutput(self):
        return self._driver.find_element_by_id("feedbackText").text

    #return the response time in ms
    def getRespTime(self):
        output = str(self.getOutput())
        # get line containing ms
        output = output.splitlines().pop(len(output.splitlines())-2)
        result = ""
        for s in output.split():
            if s.isdigit():
               result = result + s
        return int(result)

    def getRespTimeOfLexicon(self):
        try:
            print("go to settings")
            self.navigateFromFrontPageToSettings()
            print("upload own lexicon")
            self.uploadOwnLexicon()
            time.sleep(0.5)
            print("deactivating output")
            self.deactivateVerboseOutput()
            print("go to front page")
            self.navigateFromSettingsToFrontPage()
            print("creating Model...")
            self.createModel()
            time.sleep(2)
            print("setting input to our lexicon...")
            self.setInputToLexicon()
            print("run simulation...")
            self.runSimulation()
            print("finished simulation")
            time.sleep(1)
            result = self.getRespTime()
            return result

        finally:
            self._driver.quit()