from setuptools import setup, find_packages

setup(
  name = "linguistic_model_testing",
  version = "0.9.39",
  url = "https://gitlab.com/tcsvn/linguistic_model_testing",
  author = "Christian Meier",
  author_email = "webmaster@meier-lossburg.de",
  license = "MIT",
  packages = find_packages(),
  install_requires = ['selenium', 'pandas']
)

