from linguistic_model_testing.createLexica import LexiconHandler
from linguistic_model_testing.containerorchestration import Dirigent
import time
import shutil
import os
import yaml


def createDirectorys(
    pathToContLexicaFolder,
    pathToContResultFolder,
    result_file
    ):
    # create directorys inside container
    os.makedirs(pathToContLexicaFolder, exist_ok=True)
    os.makedirs(pathToContResultFolder, exist_ok=True)
    # touch the directorys
    pathToHostResultFile = pathToContLexicaFolder + '/' + result_file
    with open(pathToHostResultFile, 'a'):
        os.utime(pathToHostResultFile, None)
        #os.open(pathToHostResultFile, 'a').close()

    # clean up
    shutil.rmtree(pathToContLexicaFolder)
    shutil.rmtree(pathToContResultFolder)
    os.mkdir(pathToContLexicaFolder)
    os.mkdir(pathToContResultFolder)

def createLexicaHandler(lexx_name,
                  pathToOriginalLexicon,
                  pathToContLexicaFolder,
                  wordLength,
                  stepList):
    # create lexicas
    print('--')
    print('this is put into lexicon handler')
    print(lexx_name)
    print(pathToOriginalLexicon)
    print(pathToContLexicaFolder)

    lh = LexiconHandler(
        lexx_name=lexx_name,
        pathToOriginalLexicon=pathToOriginalLexicon,
        pathToLexicaFolder=pathToContLexicaFolder,
        stepList=stepList)

    lh.set_wordLength(wordLength)
    return lh

def runDirigent(
        pathToHostLexicaFolder,
        pathToHostResultFolder,
        pathToContResultFolder,
        stepList,
        lexx_name,
        url,
        result_file
    ):

    docker_dirigent = Dirigent(
        stepList,
        lexicon_name=lexx_name,
        url=url,
        hostPathToLexicaFolder=pathToHostLexicaFolder,
        hostPathToResultFolder=pathToHostResultFolder,
        result_file=result_file)
    print(docker_dirigent)
    #docker_dirigent.build_worker_image()
    print('create containers ...')
    docker_dirigent.create_containers()
    print('start containers ...')
    print(str(docker_dirigent))
    print("-"*50)
    docker_dirigent.start_containers()

#------------------------- main ------------------------------------------------------
if __name__ == "__main__":
    # read in details
    cwd = os.getcwd()
    path_config = cwd + '/config.yaml'
    with open(path_config, 'r') as ymlfile:
        cfg_dump = yaml.load(ymlfile)["lexx"]
        lexx_name = cfg_dump['name']
        pathToOriginalLexicon = cfg_dump['pathToOriginalLexicon']
        wordLength = cfg_dump['wordLength']
        try:
            stepList = cfg_dump['stepList']
            isUsingStepList=True
        except:
            isUsingStepList=False
            print('not using stepList generating one')

            stepsize = cfg_dump['generate_steps']['stepsize']
            first = cfg_dump['generate_steps']['first']
            last = cfg_dump['generate_steps']['last']
            stepList = []
            for number in range(first, last+stepsize, stepsize):
                stepList.append(number)

        url = cfg_dump['url']
        sampling_size = cfg_dump['sampling_size']
        # path to files
        pathToHostLexicaFolder = cfg_dump['pathHostToLexicaFolder']
        pathToHostResultFolder = cfg_dump['pathHostToResultFolder']
        pathToContLexicaFolder = '/lexica'
        pathToContResultFolder = '/results'
        pathToHostLexicaFolder = pathToHostLexicaFolder + '/' + lexx_name
        pathToHostResultFolder = pathToHostResultFolder + '/' + lexx_name
        pathToContLexicaFolder = pathToContLexicaFolder + '/' + lexx_name
        pathToContResultFolder = pathToContResultFolder + '/' + lexx_name

    # logic
    createDirectorys(
        pathToContLexicaFolder,
        pathToContResultFolder,
        lexx_name)

    lexica_handler = createLexicaHandler(
            pathToOriginalLexicon=pathToOriginalLexicon,
            pathToContLexicaFolder=pathToContLexicaFolder,
            wordLength=wordLength,
            stepList=stepList,
            lexx_name=lexx_name)

    for i in range(0, sampling_size):
        result_file = 'result_' + str(i) + '.csv'
        lexica_handler.generateLexicas()
        runDirigent(
            stepList=stepList,
            url=url,
            pathToHostLexicaFolder=pathToHostLexicaFolder,
            pathToHostResultFolder=pathToHostResultFolder,
            pathToContResultFolder=pathToContResultFolder,
            lexx_name=lexx_name,
            result_file=result_file
        )

        lexica_handler.deleteLexicas()
