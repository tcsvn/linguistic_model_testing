from linguistic_model_testing.worker.singleWebDriver import JIAMWebDriver

# parse command line arguments



url='http://www.psychology.nottingham.ac.uk/staff/wvh/jiam/startjiam.html'
path_to_lexicon = "/home/chris/PycharmProjects/linguistic/lexica/lexx_3to6/lexx_3to6_10.txt"
path_to_output = "/home/chris/PycharmProjects/linguistic/results/lexx_3to6/result_1.txt"
word_count = 10


temp = []
temp = path_to_lexicon.split('/')
lexicon_name = temp[len(temp)-1]
path_to_lexicon = path_to_lexicon[:-len(lexicon_name)-1]

# init webdriver
webdriver = JIAMWebDriver(url=url,
                            lexicon_name=lexicon_name,
                            path_to_lexicon=path_to_lexicon,
                            path_to_output=path_to_output,
                            headless=True)
#                            headless=False)
print(webdriver)

# get times
time = webdriver.getRespTimeOfLexicon()
# process results
print("="*90)
print("result: " + str(time))
print("writing to file...")

# wordcount time
# format 100 1290390'
line = str(word_count) + " " + str(time) + "\n"

with open(path_to_output, "a") as file:
    file.write(line)
print("successful finished work")