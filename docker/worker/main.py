from linguistic_model_testing.worker.singleWebDriver import JIAMWebDriver
import argparse
import os

# parse command line arguments
parser = argparse.ArgumentParser(description="working on linguistic stuff")
parser.add_argument("-u", "--url",
                    type=str,
                    help="url of the site where JIAM is hosted")
parser.add_argument('-pl',
                    type=str,
                    default=20,
                    help="the path to the lexicon")
parser.add_argument('-po',
                    type=str,
                    default=10,
                    help="the path to the output file")
parser.add_argument('-wc', "--wordcount",
                    type=int,
                    default=10,
                    help="the amount of words the worker processes")
args = parser.parse_args()

if args.url:
    url = args.url
if args.pl:
    path_to_lexicon = args.pl
if args.po:
    path_to_output = args.po
if args.wordcount:
    word_count = args.wordcount

if not (args.po and args.pl and args.url):
    raise Exception


temp = []
temp = path_to_lexicon.split('/')
lexicon_name = temp[len(temp)-1]
path_to_lexicon = path_to_lexicon[:-len(lexicon_name)-1]

#url='http://www.psychology.nottingham.ac.uk/staff/wvh/jiam/startjiam.html'
#path_to_lexicon = "/data/lexx3to6_100.txt"
#path_to_output = "/result/result.txt"
#lexicon_name="lexx3to6_100.txt"

# init webdriver
webdriver = JIAMWebDriver(url=url,
                            lexicon_name=lexicon_name,
                            path_to_lexicon=path_to_lexicon,
                            path_to_output=path_to_output,
                            headless=True)
#                            headless=False)
print(webdriver)

# get times
time = webdriver.getRespTimeOfLexicon()
# process results
print("="*90)
print("result: " + str(time))
print("writing to file...")

# wordcount time
# format 100 1290390'
line = str(word_count) + " " + str(time) + "\n"

with open(path_to_output, "a") as file:
    file.write(line)
print("successful finished work")