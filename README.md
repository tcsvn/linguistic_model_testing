# Testing of Interactive Activation Model 

### Status
[![pipeline status](https://gitlab.com/tcsvn/linguistic_model_testing/badges/master/pipeline.svg)](https://gitlab.com/tcsvn/linguistic_model_testing/commits/master)

---
### Getting Started
These instructions will get you a copy of the project up and running on your local machine either for development and testing or deployment purposes.

#### Requirements
* the latest version of [docker](https://www.docker.com/community-edition#/download)
* the latest version of [docker-compose](https://docs.docker.com/compose/install/)

#### Installation
- fetch the directory from this site with `git clone git@gitlab.com:tcsvn/linguistic_model_testing.git` 
```
cd linguistic_model_testing
make install
```
- on linux navigate into the directory and run: `make install`
- on windows look at the content of the `Makefile` and execute the commands

#### Deplpoyment
- Set your preferred settings in `config.yaml`
- Execute the command `docker-compose up -d`
- The results of the *run* should be in the folder `results/lexx_(name)/results.txt` where `(name)` is the name you specified in the `config.yaml` 

---
#### Development
to build the test package in the root directory of the project run 
` pip install .`
to import the package the following code is required `import linguistic_model_testing` 
Both *worker* and *orchestrator* of docker are located inside subdirectorys of `$pwd/docker`

#### Contributing
Please read CONTRIBUTING.md for details on our code of conduct, and the process for submitting pull requests to us.

---
## Authors

* **Christian Meier**

See also the list of [contributors](linktocontributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details